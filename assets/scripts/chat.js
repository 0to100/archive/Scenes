function initChat() {
    let app = new Vue({
        el: '#chat',
        data: {
            messages: []
        },
        mounted() {
            const socket = io('http://192.168.1.80:3000');
            socket.on('message', (data) => {
                let {channel, tags, message, self} = data
                let emoteMessage = [];
                let removedLength = 0
                let emotesArray = []
                if (tags.emotes) emotesArray = Object.entries(tags.emotes);
                emotesArray = emotesArray
                    .map(emote => {
                        let arr = [];
                        emote[1].forEach(position => {
                            arr.push({
                                emote: emote[0],
                                position: position
                            })
                        })
                        return arr;
                    })
                    .flat()
                    .sort((a, b) => {
                        return a.position.split('-')[0] - b.position.split('-')[0]
                    })
                emotesArray.forEach(emote => {
                    const [start, end] = emote.position.split('-')
                    emoteMessage.push({
                        type: 'text',
                        message: message.slice(0, +start - removedLength)
                    })
                    emoteMessage.push({
                        type: 'img',
                        source: `https://static-cdn.jtvnw.net/emoticons/v1/${emote.emote}/1.0`
                    })
                    message = message.slice(+end + 1 - removedLength, message.length)
                    removedLength = +end + 1
                })
                emoteMessage.push({
                    type: 'text',
                    message: message
                })
                this.messages.unshift({
                    id: tags.id,
                    content: emoteMessage,
                    author: tags['display-name']
                })
                if (this.messages.length > 20) {
                    this.messages.pop()
                }
            });

            socket.on('messagedeleted', (messageId) => {
                const index = this.messages.findIndex(message => {
                    return message.id === messageId;
                })
                this.messages.splice(index, 1)
            })
        }
    })
}

initChat()