function initTTT() {
    new Vue({
        el: '#ttt',
        data: {
            player1: null,
            player2: null,
            gameState: {
                1: null,
                2: null,
                3: null,
                4: null,
                5: null,
                6: null,
                7: null,
                8: null,
                9: null,
            },
            gameStarted: false,
            player1Turn: true,
            winner: null
        },
        mounted() {
            const socket = io('http://192.168.1.80:3000');
            socket.on('ttt', (data) => {
                const {channel, tags, message, self} = data
                const field = parseInt(message.split(' ')[1]);
                if (1 <= field && field <= 9) {
                    if (this.gameStarted && !this.winner) {
                        if (tags.username === this.currentPlayer && !this.gameState[field]) {
                            this.gameState[field] = tags.username;
                            this.checkForWin();
                            this.player1Turn = !this.player1Turn;
                        }
                        // TODO: Sollte ein Spieler nicht innerhalb von 30s spielen gewinnt der andere
                    } else {
                        if (this.player1) {
                            if (this.player1 !== tags.username) {
                                this.player2 = tags.username;
                                this.gameStarted = true;
                                if (tags.username === this.currentPlayer && !this.gameState[field]) {
                                    this.gameState[field] = tags.username;
                                    this.player1Turn = true;
                                }
                            }
                        } else {
                            this.player1 = tags.username;
                            this.gameState[field] = tags.username;
                            this.player1Turn = false;
                        }
                    }
                }
            })
        },
        computed: {
            currentPlayer() {
                return this.player1Turn ? this.player1 : this.player2
            }
        },
        methods: {
            checkForWin() {
                let win = false;
                for (let row = 1; row < 4; row++) {
                    if (this.gameState[row * 3 - 2] === this.currentPlayer && this.gameState[row * 3 - 1] === this.currentPlayer && this.gameState[row * 3] === this.currentPlayer) {
                        this.setWinner()
                        win = true
                    }
                    if (this.gameState[row] === this.currentPlayer && this.gameState[row + 3] === this.currentPlayer && this.gameState[row + 6] === this.currentPlayer) {
                        this.setWinner()
                        win = true
                    }
                }
                if ((this.gameState[1] === this.currentPlayer && this.gameState[5] === this.currentPlayer && this.gameState[9] === this.currentPlayer)) {
                    this.setWinner()
                    win = true
                }
                if ((this.gameState[3] === this.currentPlayer && this.gameState[5] === this.currentPlayer && this.gameState[7] === this.currentPlayer)) {
                    this.setWinner()
                    win = true
                }
                let boardFull = true;
                for (let i = 1; i <= 9; i++) {
                    if (!this.gameState[i]) boardFull = false;
                }
                if (!win && boardFull) this.resetGame()
            },
            setWinner() {
                this.winner = this.currentPlayer;
                this.gameStarted = false;
                setTimeout(this.resetGame, 10000)
            },
            resetGame() {
                this.player1 = null;
                this.player2 = null;
                this.gameState = {
                    1: null,
                    2: null,
                    3: null,
                    4: null,
                    5: null,
                    6: null,
                    7: null,
                    8: null,
                    9: null,
                };
                this.gameStarted = false;
                this.player1Turn = true;
                this.winner = null;
            }
        }
    })
}

initTTT()