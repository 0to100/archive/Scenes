function initSign() {
    let str = document.getElementById('neon').innerText;
    document.getElementById('neon').innerText = '';
    let arr = str.split('');
    let els = [];
    arr.forEach(char => {
        let span = document.createElement('span');
        span.innerText = char;
        document.querySelector('#neon').appendChild(span);
        els.push(span)
    });
    setInterval(() => {
        let num = getRandomInt(0, els.length - 1);
        els[num].classList.add('animate')
        setTimeout(() => {
            els[num].classList.remove('animate')
        }, 10000)
    }, 10000);
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

initSign()